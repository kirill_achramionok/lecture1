package com.company.operator;

/**
 * @author kirill.akhramenok
 */
public class Main {
    public static void main(String[] args) {
        prefixAndPostfixExample();
    }

    private static void prefixAndPostfixExample() {
        int first = 0;
        int second = 0;
        first = second++;
        System.out.println(second++);
        System.out.println(--first);
    }
}
