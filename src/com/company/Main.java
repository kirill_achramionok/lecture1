package com.company;

import java.util.Scanner;

/**
 * @author kirill.akhramenok
 */
public class Main {

    public static void main(String[] args) {
//        System.out.println("Hello World!");
//        readFromConsole();
//        printArgsExample(args);
        switchCaseExample();
//        autoboxingUnBoxingExample();
////        doWhileExample();
    }

    private static void autoboxingUnBoxingExample() {
        Integer iOb = 7;
        Double dOb = 7.0;
        Character cOb = 'a';
        Boolean bOb = true;

        int i = iOb;
        double d = dOb;
        char c = cOb;
        boolean b = bOb;
        Integer iOb1 = 7;
        Integer iOb2 = 7;
        System.out.println(iOb1 > iOb2);
        System.out.println(iOb1 + iOb2);

        System.out.println(iOb1 == iOb2);

        Integer iOb3 = new Integer(120);
        Integer iOb4 = new Integer(120);
        System.out.println(iOb3 == iOb4);

        Integer iOb5 = 200;
        Integer iOb6 = 200;
        System.out.println(iOb5 == iOb6);
    }

    private static void readFromConsole() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(scanner.next());
    }

    private static void printArgsExample(String[] arguments) {
        for (String argument : arguments) {
            System.out.println(argument);
        }
    }

    private static void switchCaseExample() {
        Scanner scanner = new Scanner(System.in);
        String value;
        while (!(value = scanner.next()).equals("exit")) {
            switch (value) {
                case "1":
                    System.out.println(1);
                    break;
                case "0":
                    System.out.println(0);
                case "2":
                    System.out.println(2);
                case "3":
                    System.out.println(3);

            }
        }
    }

    private static void conditionOperators() {
        int z = 10;
        if (z > 0) {
            System.out.println("Позитивное");
        } else if (z == 0) {
            System.out.println("Ноль");
        } else if (z < 0) {
            System.out.println("Негативное");
        }
    }

    private static void doWhileExample() {
        int i = 0;
        for (i = 0; i < 1; i++) {
            System.out.println(i);

        }
        i = 0;
        while (i != 0) {
            System.out.println("While");
        }
        do {
            System.out.println("Do while");
        } while (i != 0);
    }
}


