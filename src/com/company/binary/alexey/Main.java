package com.company.binary.alexey;

/**
 * @author kirill.akhramenok
 */
public class Main {
    public static void main(String[] args) {
        FileProcessor fileProcessor = new DocProcessor();
        process(fileProcessor);
    }

    public static void process(FileProcessor fileProcessor){
        fileProcessor.processFile();
    }
}
