package com.company.binary.alexey;

/**
 * @author kirill.akhramenok
 */
public interface FileProcessor {
    void processFile();
}
