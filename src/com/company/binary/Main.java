package com.company.binary;

/**
 * @author kirill.akhramenok
 */
public class Main {
    private static final String NIKITA_DIMA = "";

    public static void main(String[] args) {
//        anagramExample();
        swapExample();
    }

    private static void anagramExample() {
        String firstString = "колба";
        String secondString = "бокал";
        char[] firstStringChars = firstString.toLowerCase().toCharArray();
        char[] secondStringChars = secondString.toLowerCase().toCharArray();
        int result = firstStringChars[0] ^ secondStringChars[0];
        if (firstString.length() == secondString.length()) {
            for (int i = 1; i < firstString.length(); i++) {
                result = firstStringChars[i] ^ secondStringChars[i] ^ result;
            }
        }
        String textResult = result == 0 ? "True" : "False";
        System.out.println(textResult);
    }

    private static void swapExample() {
        int x = 1;
        int y = 2;
        System.out.println("x = " + x + ", y = " + y);
        x = x ^ y;
        y = x ^ y;
        x = x ^ y;
        System.out.println("x = " + x + ", y = " + y);
    }
}
